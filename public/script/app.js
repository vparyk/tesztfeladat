var app = angular.module('app', [])
    .controller('ListController', function ($scope, $http) {
         $scope.select=[];
         $scope.select1=[];
         $scope.select2=[];

        $http.get("/keywordList").then(function (response) {
            $scope.obj = response.data;

            for(var key1 in $scope.obj) {
                if ($scope.obj.hasOwnProperty(key1)) {
                    $scope.select.push(key1);
                    $scope.select1.push(key1);
                    $scope.select1[key1]=[];

                for(var key2 in $scope.obj[key1]) {
                    if ($scope.obj[key1].hasOwnProperty(key2)) {
                        $scope.select2.push(key2);
                        $scope.select2[key2]=[];

                        $scope.select1[key1].push(key2);
                        for (var key3 in $scope.obj[key1][key2]) {
                            if ($scope.obj[key1][key2].hasOwnProperty(key3)) {
                                $scope.select2[key2].push($scope.obj[key1][key2][key3]);

                            }
                        }
                    }
                }
                }
            }
        });

        $scope.getPictures = function (search) {
            var config = {
                params: {
                    "search" : search,
                    "tags" : $scope.selected1 ? $scope.selected1 : "" + " "+ $scope.selected2 ? $scope.selected2 : ""  +" "+$scope.selected3 ? $scope.selected3 : ""
                }
            };
            $http.get("/pictures", config).then(function (response) {
                $scope.photos = response.data;
            });
        }
    });