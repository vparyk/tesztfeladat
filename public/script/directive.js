app
    .directive('listImages', function () {
        return {
            //Points to the URL that loads a snipet of appropriate view
            templateUrl: 'template/photos.html'
        };
    })
    .directive('header', function () {
        return {
            //Points to the URL that loads a snipet of appropriate view
            templateUrl: 'template/header.html'
        };
    })
    .directive('keywords', function () {
        return {
            //Points to the URL that loads a snipet of appropriate view
            templateUrl: 'template/keywords.html'
        };
    });