var express = require('express');
var app = express();
var customKey = require('./custom-key');

app.use(express.static(__dirname + "/public"));

app.get('/keywordList', function (req, res) {
    res.json(customKey.readKeyFromFile());
});

app.get('/pictures', function (req, res) {
    var Flickr = require("flickrapi"),
        flickrOptions = {
            api_key: "57f694132e4714c29a64c9af890b124e",
        };

    Flickr.tokenOnly(flickrOptions, function (error, flickr) {
        // we can now use "flickr" as our API object,
        // but we can only call public methods and access public data
        flickr.photos.search({
            user_id: flickr.options.user_id,
            page: 1,
            per_page: 100,
            extras: 'url_m',
            tags: req.query.tags,
            text: req.query.search
        }, function (err, result) {
            // result is Flickr's response
            res.json(result);
        });
    });

});

app.listen(3000);
console.log("Server is runing");
